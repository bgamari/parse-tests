module Main where

import Data.Either

import System.FilePath.Glob

import GhcTests.Parse

main :: IO ()
main = do
  tFiles <- glob "../**/*.T"
  (fails, goods) <- partitionEithers <$> mapM parseFile tFiles
  writeFile "tests" $ unlines $ map show $ concat goods
  mapM_ print fails

