{-# LANGUAGE PatternSynonyms #-}
{-# LANGUAGE ViewPatterns #-}

module GhcTests.Parse (parseFile) where

import Data.Either

import Language.Python.Common.AST
import Language.Python.Version3.Parser (parseModule)

import Control.Monad.Trans.RWS

import GhcTests.Types

tryParse :: FilePath -> IO (Either String ModuleSpan)
tryParse fname = do
  contents <- readFile fname
  return $ case parseModule contents fname of
    Left err -> Left $ show err
    Right (ms, _) -> Right ms

parseFile :: FilePath -> IO (Either String [Test])
parseFile fname = fmap parseT <$> tryParse fname

type ParseM = RWS () [Test] [Modifier]

parseT :: ModuleSpan -> [Test]
parseT (Module stmts) =
  snd $ evalRWS (mapM parseStmt stmts) () []
  where
    parseStmt :: StatementSpan -> ParseM ()
    parseStmt (StmtExpr e _span)
      | KnownCall "setTestOpts" args <- e
      , [ArgExpr arg _] <- args
      = put (parseModifiers arg)

      | KnownCall "test" args <- e
      , Just [name_e, mods, ty, ty_args] <- onlyPositional args
      , Strings [nm] _ <- name_e
      = do globalMods <- get
           test $ Test nm (globalMods <> parseModifiers mods) (parseTestType ty ty_args)
    parseStmt stmt =
      test $ TestUnknown "unknown statement" stmt

    test :: Test -> ParseM ()
    test x = tell [x]

parseTestType :: ExprSpan -> ExprSpan -> TestType
parseTestType (Id "compile_and_run") (List [String hcOpts] _) =
  CompileAndRun hcOpts
parseTestType (Id "compile") (List [String hcOpts] _) =
  Compile hcOpts
parseTestType (Id "compile_fail") (List [String hcOpts] _) =
  CompileFail hcOpts
parseTestType (Id "compile_grep_core") (List [String hcOpts] _) =
  CompileGrepCore hcOpts
parseTestType (Id "multi_compile") (List [String topMod, List files _, String hcOpts] _)
  | Just files' <- sequence
      [ case f of
          Paren (Tuple [String fname, String args] _) _ -> Just (fname, args)
          _ -> Nothing
      | f <- files
      ]
  = MultiCompile topMod files' hcOpts
parseTestType (Id "multimod_compile") (List [String topMod, String hcOpts] _) =
  MultiModCompile topMod hcOpts
parseTestType (Id "multimod_compile_and_run") (List [String topMod, String hcOpts] _) =
  MultiModCompileAndRun topMod hcOpts
parseTestType (Id "multimod_compile_fail") (List [String topMod, String hcOpts] _) =
  MultiModCompileFail topMod hcOpts
parseTestType (Id "multimod_compile_filter") (List args _)
  | [String topMod, String hcOpts, String filterWith_] <- args
  = MultiModCompileFilter topMod hcOpts filterWith_
parseTestType (Id "ghci_script") (List [String script] _) =
  GhciScript script
parseTestType (Id "backpack_run") (List [String hcOpts] _) =
  BackpackRun hcOpts
parseTestType (Id "backpack_compile") (List [String hcOpts] _) =
  BackpackCompile hcOpts
parseTestType (Id "backpack_typecheck") (List [String hcOpts] _) =
  BackpackTypecheck hcOpts
parseTestType (Id "backpack_typecheck_fail") (List [String hcOpts] _) =
  BackpackTypecheckFail hcOpts
parseTestType (Id "backpack_compile_fail") (List [String hcOpts] _) =
  BackpackCompileFail hcOpts
parseTestType (Id "run_command") (List [String cmd] _) =
  RunCommand cmd
parseTestType (Id "makefile_test") (List args _)
  | [String s] <- args = MakefileTest s
  | [] <- args = MakefileTest ""
parseTestType t args = UnknownTestType t args

parsePredicate :: ExprSpan -> Predicate
parsePredicate (KnownCall "opsys" (OnlyPositional [String o])) =
  OpSys o
parsePredicate e =
  UnknownPredicate e

simpleModifiers :: [(String, Modifier)]
simpleModifiers =
  [ "req_haddock" .-> ModReqHaddock
  , "req_profiling" .-> ModReqProfiling
  , "req_dynamic_lib_support" .-> ModReqDynamicLibSupport
  , "req_dynamic_hs" .-> ModReqDynamicHs
  , "req_dynamic_ghc" .-> ModReqDynamicGhc
  , "req_interp" .-> ModReqInterp
  , "req_bco" .-> ModReqBCO
  , "req_rts_linker" .-> ModReqRtsLinker
  , "req_c" .-> ModReqC
  , "req_cxx" .-> ModReqCxx
  , "req_cmm" .-> ModReqCmm
  , "req_ffi_exports" .-> ModReqFfiExports
  , "req_asm" .-> ModReqAsm
  , "req_th" .-> ModReqTH
  , "req_ghc_smp" .-> ModReqGhcSmp
  , "req_target_smp" .-> ModReqTargetSmp
  , "req_process" .-> ModReqProcess
  , "req_host_target_ghc" .-> ModReqHostTargetGhc
  , "req_ppr_deps" .-> ModReqPprDeps

  , "expect_fail" .-> ModExpectFail
  , "ignore_stderr" .-> ModIgnoreStderr
  , "ignore_stdout" .-> ModIgnoreStdout
  , "no_lint" .-> ModNoLint
  , "skip" .-> ModSkip
  , "js_skip" .-> ModJsSkip
  , "windows_skip" .-> ModWindowsSkip
  , "combined_output" .-> ModCombinedOutput
  , "normalise_slashes" .-> ModNormaliseSlashes
  , "only_ghci" .-> ModOnlyGhci
  , "copy_files" .-> ModCopyFiles
  ]
  where (.->) = (,)

parseModifiers :: ExprSpan -> [Modifier]
parseModifiers (List xs _) =
  foldMap parseModifiers xs
parseModifiers (Id "normal") = []
parseModifiers (Id fun)
  | Just m <- lookup fun simpleModifiers = [m]
parseModifiers (KnownCall "when" (OnlyPositional [p,k])) =
  [ModWhen (parsePredicate p) (parseModifiers k)]
parseModifiers (KnownCall "unless" (OnlyPositional [p,k])) =
  [ModUnless (parsePredicate p) (parseModifiers k)]
parseModifiers (KnownCall "extra_files" (OnlyPositional [fs]))
  | Just xs <- stringLitList fs = [ModExtraFiles xs]
parseModifiers (KnownCall "extra_ways" (OnlyPositional [fs]))
  | Just xs <- stringLitList fs = [ModExtraWays xs]
parseModifiers (KnownCall "only_ways" (OnlyPositional [fs]))
  | Just xs <- stringLitList fs = [ModOnlyWays xs]
parseModifiers (KnownCall "omit_ways" (OnlyPositional [fs]))
  | Just xs <- stringLitList fs = [ModOmitWays xs]
parseModifiers (KnownCall "normalise_version" (OnlyPositional args))
  | Just pkgs <- mapM (\arg -> do {String pkg <- Just arg; return pkg}) args = [ModNormaliseVersion pkgs]
parseModifiers (KnownCall "grep_errmsg" (OnlyPositional [String needle])) = [ModGrepErrmsg needle]
parseModifiers (KnownCall "check_errmsg" (OnlyPositional [String needle])) = [ModCheckErrmsg needle]
parseModifiers (KnownCall "pre_cmd" (OnlyPositional [String cmd])) = [ModPreCmd cmd]
parseModifiers (KnownCall "extra_hc_opts" (OnlyPositional [String opts])) = [ModExtraHcOpts opts]
parseModifiers (KnownCall "extra_run_opts" (OnlyPositional [String opts])) = [ModExtraRunOpts opts]
parseModifiers (KnownCall "expect_broken" (OnlyPositional [Int n _ _])) = [ModExpectBroken n]
parseModifiers (KnownCall "js_broken" (OnlyPositional [Int n _ _])) = [ModJsBroken n]
parseModifiers (KnownCall "fragile" (OnlyPositional [Int n _ _])) = [ModFragile n]
parseModifiers (KnownCall "fragile_for" (OnlyPositional [Int n _ _, strs]))
  | Just ways <- stringLitList strs = [ModFragileFor n ways]
parseModifiers (KnownCall "js_fragile" (OnlyPositional [Int n _ _])) = [ModJsFragile n]
parseModifiers (KnownCall "exit_code" (OnlyPositional [Int n _ _])) = [ModExitCode n]
parseModifiers (KnownCall "collect_compiler_stats" (OnlyPositional [String stat, Int n _ _])) = [ModCollectCompilerStats stat (fromIntegral n)]
-- TODO timeout_multiplier
-- TODO run_timeout_multiplier
-- TODO cmd_prefix
-- TODO pre_cmd_timeout_multiplier
-- TODO {c,cxx,objc,objcxx,cmm}_src
parseModifiers e =
  [UnknownModifier e]

pattern KnownCall :: String -> [Argument a] -> Expr a
pattern KnownCall fn args <- Call (Id fn) args _

-- | A plain variable
pattern Id :: String -> Expr a
pattern Id n <- Var (Ident n _) _

pattern String :: String -> Expr a
pattern String ss <- Strings (concat -> ss) _

pattern OnlyPositional :: [Expr a] -> [Argument a]
pattern OnlyPositional es <- (onlyPositional -> Just es)

onlyPositional :: [Argument a] -> Maybe [Expr a]
onlyPositional args
  | ([], es) <- partitionEithers $ map f args = Just es
  | otherwise                                 = Nothing
  where
    f (ArgExpr e _) = Right e
    f _             = Left ()

stringLitList :: Expr a -> Maybe [String]
stringLitList (List xs _) = mapM (\e -> do String s <- Just e; return s) xs
stringLitList _ = Nothing

