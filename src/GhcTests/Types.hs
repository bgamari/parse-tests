module GhcTests.Types where

import Language.Python.Common.AST

data Predicate
  = OpSys String
  | UnknownPredicate ExprSpan
  deriving (Show)

data Modifier
  = ModWhen Predicate [Modifier]
  | ModUnless Predicate [Modifier]
  | ModExtraFiles [String]
  | ModExtraWays [String]
  | ModOnlyWays [String]
  | ModOmitWays [String]
  | ModJsBroken Integer
  | ModExpectBroken Integer
  | ModFragile Integer
  | ModFragileFor Integer [String]
  | ModJsFragile Integer
  | ModExitCode Integer
  | ModNormaliseVersion [String]
  | ModExtraHcOpts String
  | ModExtraRunOpts String
  | ModGrepErrmsg String
  | ModCheckErrmsg String
  | ModPreCmd String
  | ModCollectCompilerStats String Double

  | ModReqHaddock
  | ModReqProfiling
  | ModReqDynamicLibSupport
  | ModReqDynamicHs
  | ModReqDynamicGhc
  | ModReqInterp
  | ModReqBCO
  | ModReqRtsLinker
  | ModReqC
  | ModReqCxx
  | ModReqCmm
  | ModReqFfiExports
  | ModReqAsm
  | ModReqTH
  | ModReqGhcSmp
  | ModReqTargetSmp
  | ModReqProcess
  | ModReqHostTargetGhc
  | ModReqPprDeps

  | ModNormaliseSlashes
  | ModExpectFail
  | ModIgnoreStderr
  | ModIgnoreStdout
  | ModNoLint
  | ModSkip
  | ModJsSkip
  | ModWindowsSkip
  | ModCombinedOutput
  | ModOnlyGhci
  | ModCopyFiles

  | UnknownModifier ExprSpan
  deriving (Show)

data TestType
  = Compile { extraHcOpts :: String }
  | CompileFail { extraHcOpts :: String }
  | CompileAndRun { extraHcOpts :: String }
  | CompileGrepCore { extraHcOpts :: String }
  | MultiCompile { topModule :: String, mcFiles :: [(FilePath, String)], extraHcOpts :: String }
  | MultiModCompile { topModule :: String, extraHcOpts :: String }
  | MultiModCompileAndRun { topModule :: String, extraHcOpts :: String }
  | MultiModCompileFail { topModule :: String, extraHcOpts :: String }
  | MultiModCompileFilter { topModule :: String, extraHcOpts :: String, filterWith :: String }
  | BackpackCompile { extraHcOpts :: String }
  | BackpackCompileFail { extraHcOpts :: String }
  | BackpackTypecheck { extraHcOpts :: String }
  | BackpackTypecheckFail { extraHcOpts :: String }
  | BackpackRun         { extraHcOpts :: String }
  | GhciScript { ghciScript :: String }
  | RunCommand { command :: String }
  | MakefileTest { makeOpts :: String }
  | UnknownTestType ExprSpan ExprSpan
  deriving (Show)

data Test
  = Test { testName :: String
         , testModifiers :: [Modifier]
         , testType :: TestType
         }
  | TestUnknown { testWhat :: String
                , testSpan :: StatementSpan
                }
  deriving (Show)
