# parse-tests

This is a parser of the GHC testsuite's Python test definition syntax, intended to facilitate limited automated refactoring of GHC's testsuite into a more declarative, self-contained form as proposed in [GHC#23985](https://gitlab.haskell.org/ghc/ghc/-/issues/23985).